import React, {Component, Fragment} from 'react';

import {connect} from 'react-redux'
import {Button, Col, Form, FormGroup, Input, ListGroup, ListGroupItem,} from "reactstrap";
import {addTodo, fetchTodo} from "../store/actions";
import Spinner from "../components/UI/Spinner/Spinner";



class FormAddTodoList extends Component {
    state = {
        text: ''
    };
    valueChange = event => {
        this.setState({
            text: event.target.value
        })
    };

    componentDidMount() {
        this.props.fetchTodo();
    }

render() {
        const lists =  Object.keys(this.props.todoList).map(text => (
            <ListGroup key={text}>
                <ListGroupItem className="mt-4">{this.props.todoList[text].text}</ListGroupItem>
            </ListGroup>

        ));


        return (
            <Fragment>

            <Form>
                <FormGroup row>
                    <Col sm={11}>
                        <Input value={this.props.value} onChange={this.valueChange} type="text" name="text" />
                    </Col>
                    <Button onClick={() => this.props.addTodo(this.state.text)}>add</Button>
                </FormGroup>
            </Form>
                {this.props.loading ?  <Spinner className="text-center"/> : lists}
            </Fragment>

        );
    }
}

const mapStateToProps = state => {
    return {
        loading: state.loading,
        todoList: state.todoList
    }
};



const mapDispatchToProps = dispatch => {
    return {
        addTodo: (text) => dispatch(addTodo(text)),
        fetchTodo: () => dispatch(fetchTodo())
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(FormAddTodoList);