import React, { Component } from 'react';
import './App.css';

import FormAddTodoList from "./containers/FormAddTodoList";
import Container from "reactstrap/es/Container";


class App extends Component {
  render() {
    return (
        <Container>
           <FormAddTodoList/>
        </Container>

    );
  }
}

export default App;
