import {FETCH_TODO_REQUEST, FETCH_TODO_SUCCESS,} from "./actions";


const initialState = {
    loading: false,
    todoList: []
};

const reducer = (state = initialState, action) => {

    switch (action.type) {
        case FETCH_TODO_REQUEST:
            return {
                ...state,
                loading: true
            };
        case FETCH_TODO_SUCCESS:
            return {
                ...state,
                todoList: action.todo,
                loading: false
            };

        default:
            return state
    }



};

export default reducer;