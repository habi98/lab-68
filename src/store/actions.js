import axios from '../axios-todo'

export const VALUE_CHANGE = 'VALUE_CHANGE';

export const FETCH_TODO_SUCCESS = 'FETCH_TODO_SUCCESS';
export const FETCH_TODO_REQUEST = 'FETCH_TODO_REQUEST';

export const  fetchDotoListRequest = () => {
    return {type:  FETCH_TODO_REQUEST }
};
export const  fetchDotoListSuccess = (todo) => {
    return {type:  FETCH_TODO_SUCCESS  , todo}
};


export const fetchTodo = () => {
   return dispatch => {
       dispatch(fetchDotoListRequest());
       axios.get('/todo.json').then(response => {
           dispatch(fetchDotoListSuccess(response.data))
       })
   }
};

export const addTodo = (text) => {
    return (dispatch) => {
        axios.post('/todo.json', {text}).then(() => {
            dispatch(fetchTodo())
        })
    }
};